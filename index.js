/* eslint-disable array-callback-return */
/* eslint-disable node/no-deprecated-api */
const cheerio = require('cheerio')
const loadJsonFile = require('load-json-file')
const axios = require('axios')
const URL = require('url')

const fetch = async (url) => {
  try {
    const { data } = await axios.get(url)
    return data
  } catch (error) {
    await new Promise(resolve => setTimeout(resolve, 10000))
    return false
  }
}

const URLs = async () => {
  let urls = []
  for (let index = 1; index <= 9; index++) {
    console.log('🚀 ~ index', index)
    const url = 'http://valvrareteam.com/page/' + index
    const html = await fetch(url)
    const $ = cheerio.load(html)
    urls = [...urls, ...$('.front-view-title a').map(function list () {
      return `${$(this).attr('href')}`
    }).get()]
  }
  return urls
}

// URLs().then((urls) => {
//   writeJsonFile('docs/index.html', urls)
// })

const read = require('read-file')
const write = require('write-file-utf8')
const _ = require('lodash')
const writeJsonFile = require('write-json-file')

const save = async () => {
  const urls = await loadJsonFile('docs/index.html')
  for (let i = 0; i < urls.length; i++) {
    console.log('🚀 ~ save ~ i', i)
    const url = urls[i]
    const qURL = URL.parse(url, true)
    const pathURL = 'docs' + qURL.pathname + '.html'
    try {
      const htmlFile = read.sync(pathURL)
      const html = await fetch(url)
      const $File = cheerio.load(htmlFile)
      const $ = cheerio.load(html)
      const chapterURLs = $('a').map(function chapters () {
        try {
          const chapRx = $(this).attr('href').match(/.+\/story\/.+/)
          if (chapRx) {
            return $(this).attr('href')
          }
        } catch (e) {}
      }).get()
      const chapterURLsFile = $File('a').map(function chapters () {
        try {
          const chapRx = $(this).attr('href').match(/.+\/story\/.+/)
          if (chapRx) {
            return $(this).attr('href')
          }
        } catch (e) {}
      }).get()
      const diff = _.difference(chapterURLs, chapterURLsFile)
      if (diff.length > 0) {
        await write(pathURL, html)
        for (let j = 0; j < diff.length; j++) {
          console.log('🚀 ~ j', j)
          const chapterURL = diff[j]
          const qChapterURL = URL.parse(chapterURL, true)
          const pathChapterURL = 'docs/' + qChapterURL.pathname + '.html'
          const chapterHTML = await fetch(chapterURL)
          if (!chapterHTML) {
            j--
            continue
          } else {
            await write(pathChapterURL, chapterHTML)
          }
        }
      }
    } catch (error) {
      const html = await fetch(url)
      if (!html) {
        continue
      }
      await write(pathURL, html)
      const $ = cheerio.load(html)
      const chapterURLs = $('a').map(function chapters () {
        try {
          const chapRx = $(this).attr('href').match(/.+\/story\/.+/)
          if (chapRx) {
            return $(this).attr('href')
          }
        } catch (e) {}
      }).get()
      for (let j = 0; j < chapterURLs.length; j++) {
        console.log('🚀 ~ i ~ j', i, j)
        const chapterURL = chapterURLs[j]
        const qChapterURL = URL.parse(chapterURL, true)
        const pathChapterURL = 'docs/' + qChapterURL.pathname + '.html'
        const chapterHTML = await fetch(chapterURL)
        if (!chapterHTML) {
          j--
          continue
        } else {
          await write(pathChapterURL, chapterHTML)
        }
      }
    }
  }
}

save()

module.exports = {
  URLs,
  save
}
